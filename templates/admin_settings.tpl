{form_start action='save_settings' id=$id}

<div class="pageoverflow">
  <p class="pageinput">
    <input type="submit" name="{$actionid}submit" value="{$mod->Lang('submit')}"/>
  </p>
</div>

<fieldset>
  <legend>{$mod->Lang('admin_settings_legend')}</legend>

  <div class="pageoverflow">
    <p class="pagetext">
      <label for="username">
        {$mod->Lang('admin_settings_username')}<span style="color: #e21010">*</span>
      </label>
    </p>
    <p class="pageinput">
      <input type="text" id="username" name="{$actionid}username" value="{$username}">
    </p>
  </div>

  <div class="pageoverflow">
    <p class="pagetext">
      <label for="password">
        {$mod->Lang('admin_settings_password')}<span style="color: #e21010">*</span>
      </label>
    </p>
    <p class="pageinput">
      <input type="password" id="password" name="{$actionid}password" value="{$password}">
    </p>
  </div>

  <div class="pageoverflow">
    <p class="pagetext">
      <label for="token">
        {$mod->Lang('admin_settings_token')}<span style="color: #e21010">*</span>
      </label>
    </p>
    <p class="pageinput">
      <input type="password" id="token" name="{$actionid}token" value="{$token}">
    </p>
  </div>

</fieldset>

{form_end}
