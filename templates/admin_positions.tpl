<a href="{cms_action_url action=import}">{$mod->Lang('admin_positions_import')}</a>

<div class="pageoverflow">
{*    <p class="pageoptions">{$newtablink}&nbsp;&nbsp;&nbsp;<span id="loader"> </span></p>*}
</div>

<div class="pageoverflow">
    {form_start action='edittab'}
    <table class="pagetable sort_table_tabs">
        <thead>
        <tr>
            <th class="pagew25">Title</th>
            <th>Location</th>
            <th>Start date</th>
        </tr>
        </thead>
        {foreach from=$positions item=position}
            <tr>
                <td>
                    <a href="{cms_action_url action=detail pid=$position->getInternalId()}">
                        {$position->getTitle()}
                    </a>
                </td>
                <td>{$position->getLocation()}</td>
                <td>{$position->getStartDate()}</td>
            </tr>
        {/foreach}
    </table>
    {form_end}
</div>