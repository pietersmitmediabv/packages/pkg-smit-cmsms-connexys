<?php
if (!function_exists("cmsms")) exit;
if (!$this->CheckPermission('Modify Site Preferences')) exit;

$this->SetPreference('username', $params['username']);
$this->SetPreference('password', $params['password']);
$this->SetPreference('token', $params['token']);

$this->SetMessage($this->Lang('admin_settings_updated'));
$this->RedirectToAdminTab('', null, 'admin_settings');
