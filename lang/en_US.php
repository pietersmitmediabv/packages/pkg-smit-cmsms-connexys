<?php

$lang['type_Connexys'] = 'Connexys';
$lang['type_summary'] = 'Summary';
$lang['type_detail'] = 'Detail';

$lang['submit'] = 'Submit';

$lang['admin_positions_import'] = 'Import';

$lang['admin_settings_legend'] = 'Connexys environment';
$lang['admin_settings_updated'] = 'Settings were saved successfully!';
$lang['admin_settings_missing'] = 'Missing one or more required settings';
$lang['admin_settings_username'] = 'Username';
$lang['admin_settings_password'] = 'Password';
$lang['admin_settings_token'] = 'Token';
$lang['admin_settings_detail_page_id'] = 'Detail PageId';


//$lang['admin_pages_legend'] = 'Authenticated pages';
//$lang['admin_pages_intro'] = 'When a page alias is checked here, users will be redirected to the login when visiting the page.';
//$lang['admin_pages_disabled'] = 'Authentication is disabled.';
