<?php

$positions = PositionRepository::all();

$detailpage = isset($params['detailpage']) ? $params['detailpage'] : $params['returnid'];

$default = CmsLayoutTemplate::load_dflt_by_type('Connexys::summary');
if( !is_object($default) ) {
    audit('', $this->GetName(), 'No default summary template found');
    return;
}
$template = $default->get_name();
$cache_id = 'cxs' . md5(serialize($params));
$compile_id = '';

$tpl = $smarty->CreateTemplate($this->GetTemplateResource($template), $cache_id, $compile_id, $smarty);

$tpl->assign('positions', $positions);
$tpl->assign('detailpage', $detailpage);

return $tpl->display();


