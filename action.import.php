<?php

$client = new ConnexysClient();

$client->sync();

if (array_key_exists('returnid', $params)) {
    // params contains returnid, this means it's a frontend request rather than an admin panel request
    echo 'ok';
    die();
}

return $this->RedirectToAdminTab('', null, 'admin_positions');

