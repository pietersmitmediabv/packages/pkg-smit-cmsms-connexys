<?php

if (!defined('CMS_VERSION')) exit;

$uid = cmsms()->test_state(CmsApp::STATE_INSTALL) ? 1 : get_userid();

$dict = NewDataDictionary($this->GetDb());
$taboptarray = ['mysql' => 'TYPE=MyISAM'];
$fields = "
    id I KEY AUTO,
    connexys_id C(255),
    active I,
    data TEXT,
";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix() . 'module_connexys_positions', $fields, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

# Setup summary template
try {
    $summary_template_type = new CmsLayoutTemplateType();
    $summary_template_type->set_originator($this->GetName());
    $summary_template_type->set_name('summary');
    $summary_template_type->set_dflt_flag(TRUE);
    $summary_template_type->set_lang_callback('Connexys::page_type_lang_callback');
    $summary_template_type->set_content_callback('Connexys::reset_page_type_defaults');
    $summary_template_type->set_help_callback('Connexys::template_help_callback');
    $summary_template_type->reset_content_to_factory();
    $summary_template_type->save();
} catch (CmsException $e) {
    // log it
    die(var_dump($e));
    debug_to_log(__FILE__ . ':' . __LINE__ . ' ' . $e->GetMessage());
    audit('', $this->GetName(), 'Installation Error: ' . $e->GetMessage());
}

try {
    $fn = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'example_summary.tpl';
    if (file_exists($fn)) {
        $template = @file_get_contents($fn);
        $tpl = new CmsLayoutTemplate();
        $tpl->set_name('Connexys Summary');
        $tpl->set_owner($uid);
        $tpl->set_content($template);
        $tpl->set_type($summary_template_type);
        $tpl->set_type_dflt(TRUE);
        $tpl->save();
    }
} catch (CmsException $e) {
    // log it
    debug_to_log(__FILE__ . ':' . __LINE__ . ' ' . $e->GetMessage());
    audit('', $this->GetName(), 'Installation Error: ' . $e->GetMessage());
}


# Setup detail template
try {
    $detail_template_type = new CmsLayoutTemplateType();
    $detail_template_type->set_originator($this->GetName());
    $detail_template_type->set_name('detail');
    $detail_template_type->set_dflt_flag(TRUE);
    $detail_template_type->set_lang_callback('Connexys::page_type_lang_callback');
    $detail_template_type->set_content_callback('Connexys::reset_page_type_defaults');
    $detail_template_type->set_help_callback('Connexys::template_help_callback');
    $detail_template_type->reset_content_to_factory();
    $detail_template_type->save();
} catch (CmsException $e) {
    // log it
    die(var_dump($e));
    debug_to_log(__FILE__ . ':' . __LINE__ . ' ' . $e->GetMessage());
    audit('', $this->GetName(), 'Installation Error: ' . $e->GetMessage());
}

try {
    $fn = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'example_detail.tpl';
    if (file_exists($fn)) {
        $template = @file_get_contents($fn);
        $tpl = new CmsLayoutTemplate();
        $tpl->set_name('Connexys Detail');
        $tpl->set_owner($uid);
        $tpl->set_content($template);
        $tpl->set_type($detail_template_type);
        $tpl->set_type_dflt(TRUE);
        $tpl->save();
    }
} catch (CmsException $e) {
    // log it
    debug_to_log(__FILE__ . ':' . __LINE__ . ' ' . $e->GetMessage());
    audit('', $this->GetName(), 'Installation Error: ' . $e->GetMessage());
}


