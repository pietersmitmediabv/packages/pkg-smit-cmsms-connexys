<?php

class Connexys extends CMSModule
{
    public function GetVersion() { return '0.1'; }
    public function GetName() { return 'Connexys'; }
    public function GetFriendlyName() { return 'Connexys'; }
    public function GetAdminDescription() { return 'Import positions and send applications through Connexys SOAP'; }
    public function IsPluginModule() { return true; }
    public function GetAuthor() { return 'SMIT. Digitaal Vakmanschap'; }
    public function GetAuthorEmail() { return 'support@smit.net'; }
    public function allowSmartyCaching() { return false; }
    public function VisibleToAdminUser() { return true; }
    public function HasAdmin() { return true; }
    public function GetAdminSection() { return 'content'; }

    public function InitializeFrontend()
    {
        $routes[] = new CmsRoute('connexys/import', $this->GetName(), ['action' => 'import'], true);
        $routes[] = new CmsRoute('connexys/json', $this->GetName(), ['action' => 'json'], true);

        $this->RegisterRoute('/vacatures\/(?P<pid>[a-zA-Z0-9]+)\/(?P<junk>.+)$/', ['action' => 'detail', 'returnid' => 35]);
        $this->RegisterRoute('/vacatures\/job-details\/(?P<connexys_id>[a-zA-Z0-9]+)$/', ['action' => 'find_by_connexys_id', 'returnid' => 35]);

        foreach ($routes as $route) {
            cms_route_manager::add_dynamic($route);
        }

        $this->RegisterModulePlugin();

        $this->SetParameterType('detailpage', CLEAN_INT);
        $this->SetParameterType('pid', CLEAN_STRING);
        $this->SetParameterType('junk', CLEAN_STRING);
    }

    public function GetAdminMenuItems()
    {
        $items = [];

        if ($this->CheckPermission('Modify Content')) {
            $item = new CmsAdminMenuItem();
            $item->module = $this->GetName();
            $item->section = 'content';
            $item->title = 'Vacatures';
            $item->description = 'Vacatures via Connexys';
            $item->action = 'admin_positions';
            $item->url = $this->create_url('m1_', $item->action);
            $items[] = $item;
        }

        if ($this->CheckPermission('Modify Site Preferences')) {
            $item = new CmsAdminMenuItem();
            $item->module = $this->GetName();
            $item->section = 'siteadmin';
            $item->title = 'Settings - Connexys';
            $item->description = 'Configure Connexys environment';
            $item->action = 'admin_settings';
            $item->url = $this->create_url('m1_', $item->action);
            $items[] = $item;
        }

        return $items;
    }

    public function get_pretty_url($id, $action, $returnid = '', $params = [], $inline = false)
    {
        if ($action !== 'detail' || !isset($params['pid'])) {
            return;
        }

        if (!$position = PositionRepository::getByInternalId($params['pid'])) {
            return;
        }

        return "vacatures/{$params['pid']}/" . munge_string_to_url($position->getTitle());
    }

    public static function page_type_lang_callback($str)
    {
        $mod = cms_utils::get_module('Connexys');
        if( is_object($mod) ) return $mod->Lang('type_'.$str);
    }

    public static function template_help_callback($str)
    {
        // @todo add template help
        return '';
    }

    public static function reset_page_type_defaults(CmsLayoutTemplateType $type)
    {
        if( $type->get_originator() != 'Connexys' ) throw new CmsLogicException('Cannot reset contents for this template type');

        $fn = null;
        switch( $type->get_name() ) {
            case 'summary':
                $fn = 'example_summary.tpl';
                break;

            case 'detail':
                $fn = 'orig_detail_template.tpl';
                break;
        }

        $fn = cms_join_path(__DIR__,'templates',$fn);
        if( file_exists($fn) ) return @file_get_contents($fn);
    }
}
