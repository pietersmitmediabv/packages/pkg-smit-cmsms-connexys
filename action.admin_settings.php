<?php
if (!function_exists("cmsms")) exit;
if (!$this->CheckPermission('Modify Site Preferences')) exit;

$username = $this->GetPreference('username');
$password = $this->GetPreference('password');
$token = $this->GetPreference('token');

$smarty->assign('username', $username);
$smarty->assign('password', $password);
$smarty->assign('token', $token);

echo $this->ProcessTemplate('admin_settings.tpl');
