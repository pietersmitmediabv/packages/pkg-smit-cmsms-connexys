<?php

$position = PositionRepository::get($params['connexys_id']);

// @todo 404 if no position

$default = CmsLayoutTemplate::load_dflt_by_type('Connexys::detail');
if( !is_object($default) ) {
    audit('', $this->GetName(), 'No default detail template found');
    return;
}
$template = $default->get_name();
$cache_id = 'cxs' . md5(serialize($params));
$compile_id = '';

$tpl = $smarty->CreateTemplate($this->GetTemplateResource($template), $cache_id, $compile_id, $smarty);

$tpl->assign('position', $position);

$tpl->display();

