<?php
if (!function_exists("cmsms")) exit;
if (!$this->CheckPermission('Modify Site Preferences')) exit;

$positions = PositionRepository::all();

$smarty->assign('positions', $positions);

echo $this->ProcessTemplate('admin_positions.tpl');
