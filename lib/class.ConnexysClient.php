<?php

include_once __DIR__ . '/vendor/autoload.php';

use Smit\Connexys\Client;

class ConnexysClient extends Client
{
    public function __construct()
    {
        $mod = \cms_utils::get_module('Connexys');

        $config = [
            'username' => $mod->getPreference('username'),
            'password' => $mod->getPreference('password'),
            'token' => $mod->getPreference('token'),
            'datacenter' => 'eu6',
        ];

        if (in_array(null, $config)) {
            $mod->SetMessage($mod->Lang('admin_settings_missing'));
            $mod->RedirectToAdminTab('', null, 'admin_settings');
        }

        parent::__construct($config);
    }

    public function sync()
    {
        $this->removeDeleted();
        $this->import();
    }

    public function removeDeleted()
    {
        $remoteIds = $this->getPositionIds();

        array_map(function ($position) use ($remoteIds) {
            if (!in_array($position->getId(), $remoteIds)) {
                PositionRepository::delete($position->getId());
            }
        }, PositionRepository::all());
    }

    public function import()
    {
        $positions = $this->getPositions();

        foreach ($positions as $position) {
            if (PositionRepository::exists($position->getId())) {
                PositionRepository::update($position);
            } else {
                PositionRepository::store($position);
            }
        }
    }
}
