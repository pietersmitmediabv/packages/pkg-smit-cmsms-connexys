<?php

include __DIR__ . '/vendor/autoload.php';

use Smit\Connexys\Models\Position;

class PositionRepository
{
    protected static $table = CMS_DB_PREFIX . 'module_connexys_positions';

    public static function all()
    {
        $q = 'SELECT * FROM ' . self::$table;
        $result = \cms_utils::get_db()->Execute($q);

        $positions = [];

        foreach($result->GetRows() as $row) {
            $positions[] = new Position(json_decode($row['data'], true), $row['id']);
        }

        return $positions;
    }

    public static function get($connexysId)
    {
        $q = 'SELECT * FROM ' . self::$table . ' WHERE connexys_id = ?';
        $row = \cms_utils::get_db()->GetRow($q, [$connexysId]);

        if (!$row || $row['data'] == null) {
            return null;
        }

        return new Position(json_decode($row['data'], true), $row['id']);
    }

    public static function getByInternalId($id)
    {
        $q = 'SELECT * FROM ' . self::$table . ' WHERE id = ?';
        $row = \cms_utils::get_db()->GetRow($q, [$id]);

        if (!$row || $row['data'] == null) {
            return null;
        }

        return new Position(json_decode($row['data'], true), $row['id']);
    }

    public static function exists($connexysId)
    {
        return self::get($connexysId) !== null;
    }

    public static function store($position)
    {
        // @todo log mutations
        $db = \cms_utils::get_db();
        $q = 'INSERT INTO ' . self::$table . '(connexys_id,data) VALUES (?,?)';
        $result = $db->Execute($q, [
            $position->getId(),
            json_encode($position->getData()),
        ]);

        if( !$result ) {
            return false;
        }

        return true;
    }

    public static function update($position)
    {
        // @todo log mutations
        $db = \cms_utils::get_db();
        $q = 'UPDATE ' . self::$table . ' SET data = ? WHERE connexys_id = ?';
        $result = $db->Execute($q, [
            json_encode($position->getData()),
            $position->getId(),
        ]);

        if( !$result ) {
            return false;
        }

        return true;
    }

    public static function delete($connexysId)
    {
        // @todo log mutations
        $db = \cms_utils::get_db();
        $q = 'DELETE FROM ' . self::$table . ' WHERE connexys_id = ?';
        $result = $db->Execute($q, [$connexysId]);

        if( !$result ) {
            return false;
        }

        return true;
    }
}