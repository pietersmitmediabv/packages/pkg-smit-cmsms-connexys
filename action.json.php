<?php
header('Content-Type: application/json');
$positions = array_map(function ($position) {
	return $position->getPublicData();
}, PositionRepository::all());

echo json_encode(['positions' => $positions]);

exit;
